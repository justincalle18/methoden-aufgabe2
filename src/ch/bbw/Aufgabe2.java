package ch.bbw;

import java.util.Scanner;

public class Aufgabe2 {
    Scanner input = new Scanner(System.in);
    public String stringType(){
        System.out.println("Geben Sie etwas und ich erkenne was es ist:");
        String x = input.nextLine();
        if(x.contains("@")){
            if(x.endsWith(".com")){
                System.out.println(".....E-mail");
            }
            else if(x.endsWith(".info")){
                System.out.println(".....E-mail");
            }
            else if(x.endsWith(".de")){
                System.out.println(".....E-mail");
            }
        }
        if(x.length()==10){
            if(x.contains("-")){
                System.out.println(".....Datum");
            }
        }
        if(x.length()==5){
            if(x.contains(":")){
                System.out.println(".....Uhrzeit");
            }
        }
        else if(x.length()==4){
            if(x.contains(":")){
                System.out.println(".....Uhrzeit");
            }
        }
        else{
            System.out.println("Ich konnte es doch nicht erkennen :/");
        }
        return stringType();
    }
}
